from setuptools import find_packages, setup


setup(
    name='fujin',
    version='0.3.0',
    description="A Puzzle & Dragons API server that's so fast it can only live in darkness.",
    url='https://sakubot.info',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'fujin = fujin.cli:cli'
        ],
    },
)
