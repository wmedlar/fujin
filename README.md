# Fujin

Fujin is an asynchronous API server powered by [Sanic](https://github.com/channelcat/sanic), Mongo, and NGINX, for programmatically accessing Puzzle and Dragons data.

The API is in a pre-alpha state and may change its public API at any time.

# Setup
## Local Development
Fujin requires access to hosts running Mongo and Redis. If running locally, add the following mappings to `/etc/hosts`:
```shell
127.0.0.1 database
```
otherwise use the public IP of the host you're connecting to.

You can install the API in editable mode with its dependencies with:
```shell
$ pip install -r requirements.txt -e .
```
And run in debug mode with:
```shell
$ fujin serve --debug
```
Once running you can access the API at [`localhost:8080`](http://localhost:8080).

### HTTPS
HTTPS support is handled by an Elastic Load Balancer in AWS, and does not need to be handled within the application itself.

NGINX is configured to permanently redirect HTTP requests from the ELB to HTTPS with the following snippet:
```nginx
if ($http_x_forwarded_proto = "http") {
    return 301 https://$host$request_uri;
}
```
You can find the full NGINX configuration in [`configs/nginx.conf`](configs/nginx.conf).

## Running with Docker
A [`docker-compose.yaml`](docker-compose.yaml) file is included to simplify local development. To run the api service and its dependencies, use:
```shell
$ docker-compose up --build -d
```
Once running you can access the API at [`localhost`](http://localhost).

### Hot Reload
The local code directory is mounted over the same directory within the container (`/app/fujin`) so that any changes made will be reflected without having to rebuild it. If the `--debug` flag is passed, the container [entrypoint](docker-entrypoint.sh) will monitor the `fujin/` directory for any changes (using [entr](http://entrproject.org/)) and reload the server.

There is currently a [bug in Docker for Mac](https://github.com/docker/for-mac/issues/896) that prevents this from working, however it should still work for Linux and Windows. If using Docker for Mac you can reload the server with:
```shell
$ docker-compose restart api
```

# Tips
Nothing to see here!
