KEYNAME := ssl

.PHONY: keypair
keypair: $(KEYNAME).pem $(KEYNAME).crt

$(KEYNAME).pem:
	openssl genrsa -out "$(KEYNAME).pem" 1024

$(KEYNAME).crt:
	openssl req -new -key "$(KEYNAME).pem" -out "$(KEYNAME).csr"
	openssl x509 -req -days 365 -in "$(KEYNAME).csr" -signkey "$(KEYNAME).pem" -out "$(KEYNAME).crt"
	rm -f "$(KEYNAME).csr"

.PHONY: install
install:
	pip install -r requirements.txt -r test-requirements.txt
	pip install -e .[monitor]

.PHONY: lint
lint:
	pylint fujin/

.PHONY: test
test:
	pytest
