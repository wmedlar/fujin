#!/bin/sh

contains () {
    if [ -z "$2" ]
    then echo "Usage: contains <haystack> <needle>"
    fi

    echo "$1" | grep -e "$2" > /dev/null
    return $?
}

# hot-reload if debug flag is passed to fujin run
if [ "$1" == "serve" ] && contains "$*" "--debug"
then find fujin | entr -r fujin $@
else fujin $@
fi
