"""
fujin.cli
~~~~~~~~~
Command-line interface for running and interacting with the webapp.
"""
from asyncio import get_event_loop
from json import dumps as jsonify

import click

from .app import app
from .etl import ingest_actives, ingest_evolutions, ingest_monsters
from .mongo import initialize_connection


EXISTING_PATH = click.Path(exists=True)


def include_common_options(options):
    """Include support for common options in subcommands."""

    def wrapper(command):
        """Wrap a command with common options."""
        for option in options:
            command = option(command)

        return command

    return wrapper


mongo = (
    click.option('--mongo-host', default='127.0.0.1', envvar='FUJIN_MONGO_HOST',
                 help='Hostname of the MongoDB server.'),
    click.option('--mongo-port', default=27017, type=int, envvar='FUJIN_MONGO_PORT',
                 help='Port to connect to MongoDB.'),
    click.option('--mongo-database', default='fujin', envvar='FUJIN_MONGO_DATABASE',
                 help='Database name on which to store and retrieve data.'),
)


@click.group()
@click.version_option(message='%(version)s')
def cli():
    """A Puzzle & Dragons API server that's so fast it can only live in darkness."""


@cli.command()
@click.option('--host', '-h', default='0.0.0.0', envvar='FUJIN_HOST')
@click.option('--port', '-p', default=8080, envvar='FUJIN_PORT', type=int)
@click.option('--debug', is_flag=True)
@include_common_options(mongo)
def serve(host, port, debug, mongo_host, mongo_port, mongo_database):
    """Run the API server."""
    app.config.mongo_host = mongo_host
    app.config.mongo_port = mongo_port
    app.config.mongo_database = mongo_database

    app.run(host, port, debug=debug)


@cli.command()
@click.option('--cards', type=EXISTING_PATH, metavar='/path/to/download_card_data.json')
@click.option('--skills', type=EXISTING_PATH, metavar='/path/to/download_skill_data.json')
@click.option('--region', default='na', type=click.Choice(['na', 'jp']))
@include_common_options(mongo)
def load(cards, skills, region, mongo_host, mongo_port, mongo_database):
    """Upsert data dumps into MongoDB."""
    loop = get_event_loop()

    initialize_connection(app, loop, host=mongo_host, port=mongo_port, database=mongo_database)

    log = {}

    if skills:
        log['actives'] = ingest_actives(skills, region, loop=loop)

    if cards:
        log['evolutions'] = ingest_evolutions(cards, region, loop=loop)
        # monsters should come last as embedded fields depend on the ingestion of prior collections
        log['monsters'] = ingest_monsters(cards, region, loop=loop)

    click.echo(jsonify(log))
