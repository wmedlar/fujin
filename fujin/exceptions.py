"""
fujin.exceptions
~~~~~~~~~~~~~~~~
Exception types for common API errors, as well as their handlers.
"""
import logging

from sanic.handlers import ErrorHandler

from .utils import json_response

log = logging.getLogger(__name__)


class APIException(Exception):
    """
    Base exception from which all exceptions of this package inherit.
    """
    status = 500
    message = 'An unhandled error occurred.'

    @property
    def extras(self):
        """Exposes the class name for debugging purposes."""
        class_name = type(self).__name__
        return dict(error=class_name)


class InvalidQueryParameter(APIException):
    """
    Query parameter could not be converted to an expected type.

    Query key and value can be found in the `key` and `value` attributes,
    respectively.
    """
    status = 400
    message = 'Invalid value for query parameter.'

    def __init__(self, key, value):
        super().__init__()
        self.key = key
        self.value = value

    @property
    def extras(self):
        """Exposes the troublesome key, value pair."""
        return dict(key=self.key, value=self.value)


class RecordNotFound(APIException):
    """
    No matching record could be found in the database.

    A dictionary of filters used to query this collection can be found
    in the `filters` attribute.
    """
    status = 404
    message = 'Matching record could not be found.'

    def __init__(self, filters):
        super().__init__()
        self.filters = filters

    @property
    def extras(self):
        """Exposes the parsed filters."""
        return dict(filters=self.filters)


class APIErrorHandler(ErrorHandler):
    """
    Error handler that formats APIExceptions and its subclasses into
    a structured JSON response.
    """

    def default(self, request, exception):
        if isinstance(exception, APIException):
            endpoint = f'{request.path}?{request.query_string}'
            log.exception(
                f'Request to endpoint {endpoint!r} failed.',
                exc_info=exception,
            )
            response = json_response(
                message=exception.message,
                data=exception.extras,
                status=exception.status,
                success=False,
            )
        else:
            response = super().default(request, exception)

        return response
