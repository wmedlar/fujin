"""
fujin.constants
~~~~~~~~~~~~~~~
Constants and enumerations of static values related to PAD data.
"""
# pylint: disable=bad-whitespace,missing-docstring
from .utils import LowerCaseIntEnum


class Attribute(LowerCaseIntEnum):
    FIRE  = 0
    WATER = 1
    WOOD  = 2
    LIGHT = 3
    DARK  = 4


class EvolutionType(LowerCaseIntEnum):
    STANDARD = 0
    ULTIMATE = 1
    REVERSE  = 2


class Type(LowerCaseIntEnum):
    EVO      = 0
    BALANCED = 1
    PHYSICAL = 2
    HEALER   = 3
    DRAGON   = 4
    GOD      = 5
    ATTACKER = 6
    DEVIL    = 7
    MACHINE  = 8
    AWOKEN   = 12
    ENHANCE  = 14
    VENDOR   = 15


# these contain punctuation and whitespace and can only be created dynamically
Awakening = LowerCaseIntEnum(
    'Awakening',
    [
        'ENHANCED HP',
        'ENHANCED ATTACK',
        'ENHANCED HEAL',
        'REDUCE FIRE DAMAGE',
        'REDUCE WATER DAMAGE',
        'REDUCE WOOD DAMAGE',
        'REDUCE LIGHT DAMAGE',
        'REDUCE DARK DAMAGE',
        'AUTO-RECOVER',
        'RESISTANCE-BIND',
        'RESISTANCE-DARK',
        'RESISTANCE-JAMMERS',
        'RESISTANCE-POISON',
        'ENHANCED FIRE ORBS',
        'ENHANCED WATER ORBS',
        'ENHANCED WOOD ORBS',
        'ENHANCED LIGHT ORBS',
        'ENHANCED DARK ORBS',
        'EXTEND TIME',
        'RECOVER BIND',
        'SKILL BOOST',
        'ENHANCED FIRE ATT.',
        'ENHANCED WATER ATT.',
        'ENHANCED WOOD ATT.',
        'ENHANCED LIGHT ATT.',
        'ENHANCED DARK ATT.',
        'TWO-PRONGED ATTACK',
        'RESISTANCE-SKILL LOCK',
        'ENHANCED HEART ORBS',
        'MULTI BOOST',
        'DRAGON KILLER',
        'GOD KILLER',
        'DEVIL KILLER',
        'MACHINE KILLER',
        'BALANCED KILLER',
        'ATTACKER KILLER',
        'PHYSICAL KILLER',
        'HEALER KILLER',
        'EVO KILLER',
        'AWOKEN KILLER',
        'ENHANCE KILLER',
        'VENDOR KILLER',
        'ENHANCED COMBO',
        'GUARD BREAK',
        'FOLLOW-UP ATTACK',
        'ENHANCE TEAM HP',
        'ENHANCE TEAM RCV',
        'DAMAGE VOID PIERCE',
    ],
    start=1,
)
