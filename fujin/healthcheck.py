"""
fujin.healthcheck
~~~~~~~~~~~~~~~~~
Endpoints for monitoring the status of the webapp.
"""
from sanic import Blueprint
from sanic.response import text

blueprint = Blueprint('healthcheck')


@blueprint.route('/healthz')
def healthcheck(request):
    """Simply return a 200 status code signifying the application is alive."""
    return text('', status=200)
