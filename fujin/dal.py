"""
fujin.dal
~~~~~~~~~
Base classes that allow for the simplification of writing data access layers.
"""
from abc import ABCMeta, abstractmethod
from logging import getLogger
from types import MappingProxyType
from typing import NamedTuple

from sanic.constants import HTTP_METHODS
from sanic.response import json, HTTPResponse
from sanic.views import HTTPMethodView

from .config import DEFAULT_PAGINATION
from .exceptions import InvalidQueryParameter, RecordNotFound
from .mongo import field_types, query_many, query_one
from .utils import multipattern_filter, positive_int, setdefaultattr

log = getLogger(__name__)


class ParsedArgs(NamedTuple):
    """Container for storing the result of DataAccessLayer.parse_args."""
    filters: dict
    limit: int
    offset: int


def query_parser(defaults, transforms):
    """Factory method for creating a function to parse a mapping of query parameters."""

    @staticmethod
    def parse_queries(queries):
        """Parse filters and pagination data (if supported) from a request's query parameters."""
        # passed query params will override defaults
        params = {**defaults, **queries}
        filters = {}

        for key, value in params.items():
            transform = transforms.get(key)

            if transform is None:
                continue

            try:
                filters[key] = transform(value)
            except Exception as exc:
                raise InvalidQueryParameter(key, value) from exc

        limit = filters.pop('limit', None)
        offset = filters.pop('offset', None)

        return ParsedArgs(filters, limit, offset)

    return parse_queries


class DataAccessLayer(HTTPMethodView, metaclass=ABCMeta):
    """
    HTTPMethodView subclass for simplifying the creation of data access layer views.

    This class operates similar to ORMs in that it makes use of special class variables
    for configuration. In particular, subclasses can or should define:
        __abstract__: boolean that marks a subclass as an abstract extension to this
                      class, __doctype__ is not required if this is True

        __doctype__: a motorengine document model that this view acts as an accessor for,
                     this attribute is required for a concrete subclass

        __paginate__: boolean that marks a subclass as supporting the `limit` and `offset`
                      query parameters, used for paginating query results; if enabled and
                      not otherwise overridden, a default limit of 1000 results is

        __parameter_defaults__: a mapping of fully-qualified field names to default values
                                for when no alternative value is passed in a request's
                                query parameters; if pagination is enabled the `limit` and
                                `offset` parameters can also be set

        __blacklisted_fields__: an iterable of unix-style glob patterns that evaluate to
                                fully-qualified field names of the model in __doctype__,
                                used to prevent these fields from being used as query
                                filters

        __whitelisted_fields__: an iterable of unix-style glob patterns that evaluate to
                                fully-qualified field names of the model in __doctype__,
                                used to allow these fields to be used as query filters;
                                __whitelisted_fields__ takes presidence over
                                __blacklisted_fields__, so that only a select subset of
                                fields can be exposed as filters through query parameters

    This abstract class implements a concrete asynchronous GET handler that is
    capable of querying the supplied model with any exact-match filters passed
    as query strings. If any request parameters are passed (e.g., for a route
    like '/resource/<id:int>') this view attempt to match a single object from
    the collection, otherwise all matching objects will be returned.

    If an invalid value (i.e., cannot be converted to the type of that field)
    is passed in the query parameters, a structured JSON response will be
    returned describing the erroneous field along with a 400 status code.

    Examples:
    >>> class Monster(Document):
    ...     number = IntField()
    ...     name = StringField()
    ...     attributes = ListField(StringField())
    ...
    >>> class MonstersView(DataAccessLayer):
    ...     __doctype__ = Monster
    ...     __blacklisted_fields__ = ['name']
    ...
    >>> app.add_route(MonstersView.as_view(), '/monsters')
    >>> app.run()

    With no parameters the whole queryset is returned:
    $ curl http://localhost/monsters
    [{"number": 1, "name": "Tyrra", "attributes": ["fire"]},
     {"number": 2, "name": "Tyran", "attributes": ["fire"]},
     [...],
     {"number": 3647, "name": "Master Librarian Goddess, Kali", "attributes": ["wood", "dark"]}]

    Queries on whitelisted fields perform an exact match:
    $ curl http://localhost/monsters?attributes=light
    [{"number": 13, "name": "Pterra", "attributes": ["light"]},
     {"number": 14, "name": "Pteras", "attributes": ["light"]},
     [...],
     {"number": 3589, "name": "Sunny Side Mechanical Star God, Alcyone", "attributes": ["light", "light"]}]

    While queries on blacklisted fields are ignored:
    $ curl http://localhost/monsters?name=Pandora
    [{"number": 1, "name": "Tyrra", "attributes": ["fire"]},
     {"number": 2, "name": "Tyran", "attributes": ["fire"]},
     [...],
     {"number": 3647, "name": "Master Librarian Goddess, Kali", "attributes": ["wood", "dark"]}]

    When used with `fujin.exceptions.APIErrorHandler` requests with
    unparsable parameters return a 400 Bad Request and a structured JSON
    response:
    $ curl http://localhost/monsters?number=abc --write-out "\nstatus: %{http_code}\n"
    {"message": "Invalid value for query parameter.", "success": false, "key": "number", "value": "abc"}
    status: 400

    HEAD support is also included right out of the box:
    $ curl --head http://localhost/monsters
    HTTP/1.1 200 OK
    Date: Sat, 29 Jul 2017 04:17:52 GMT
    Content-Type: application/json
    Content-Length: 123456
    Connection: keep-alive

    It even works with errors!
    $ curl --head http://localhost/monsters?number=abc
    HTTP/1.1 400 Bad Request
    Date: Sat, 29 Jul 2017 04:21:25 GMT
    Content-Type: application/json
    Content-Length: 93
    Connection: keep-alive

    You can optionally add pagination support:
    >>> class MonstersPaginatedView(MonstersView):
    ...     __paginate__ = True
    ...
    >>> app.add_route(MonstersPaginatedView.as_view(), '/monsters/paginated')
    >>> app.run()

    The defaults are a limit of 1000 results and no offset:
    $ curl http://localhost/monsters/paginated
    [{"number": 1, "name": "Tyrra", "attributes": ["fire"]},
     {"number": 2, "name": "Tyran", "attributes": ["fire"]},
     [...],
     {"number": 1000, "name":"Abyssal Hell Deity Jackal, Anubis", "attributes":["dark", "dark"]}]

    But these can be controlled with query parameters:
    $ curl http://localhost/monsters/paginated?offset=121&limit=10
    [{"number": 122, "name": "Minerva", "attributes": ["fire"]},
     {"number": 123, "name": "War Deity Minerva", "attributes":["fire", "fire"]},
     [...],
     {"number": 131, "name": "Underlord Hades", "attributes": ["dark", "dark"]}]

    A limit of zero is the same as no limit:
    $ curl http://localhost/monsters/paginated?limit=0
    [{"number": 1, "name": "Tyrra", "attributes": ["fire"]},
     {"number": 2, "name": "Tyran", "attributes": ["fire"]},
     [...],
     {"number": 3647, "name": "Master Librarian Goddess, Kali", "attributes": ["wood", "dark"]}]

    It's also possible to query a single resource:
    >>> class MonsterView(DataAccessLayer):
    ...     __doctype__ = Monster
    ...     __blacklisted_fields__ = ['*']
    ...
    >>> app.add_route(MonsterView.as_view(), '/monster/<number:int>')
    >>> app.run()

    This uses the request params to query for a single object and supports all
    the same features as querying multiple objects:
    $ curl http://localhost/monster/752
    {"number":752,"region":"na","name":"Kirin of the Aurora, Sakuya","attributes": ["light", "light"]}

    One caveat is that if the request param can't be converted to the specified
    type, Sanic will raise its own NotFound exception and create a response
    through its internal error handler:
    $ curl http://localhost/monster/abc
    Error: Requested URL /monster/abc not found
    """

    @property
    @abstractmethod
    def __doctype__(self):
        """This attribute should point to a document type used as a model for this view."""


    @classmethod
    def __init_subclass__(subcls, parser_factory=query_parser, **kwargs): # pylint: disable=bad-classmethod-argument
        """
        Eagerly parse a subclass' data access configuration to speed up runtime responses.

        In particular this method sets the following attributes:
            methods: a frozenset of all HTTP methods supported by this view
            __abstract__: set to False if it doesn't already exist, see class docstring for behavior if
                          this is True
            __paginate__: set to False if it doesn't already exist, see class docstring for behavior if
                          this is True
            __blacklisted_fields__: a frozenset of fully-qualified field names that may not be queried,
                                    see class docstring for behavior in conjunction with __whitelisted_fields__
            __whitelisted_fields__: a frozenset of fully-qualified field names that may be queried,
                                    see class docstring for behavior in conjunction with __blacklisted_fields__
            __queryable_fields__: a frozenset of fully-qualified field names and pagination options
                                  that may be passed as query parameters
            __parameter_defaults__: a read-only mapping of fields to their default values, if an alternative
                                    is not passed in the query parameters

        And the following methods:
            parse_queries: function that takes one argument, a mapping of query parameter fields to string
                           values, that should parse the values to the correct types and return them in a
                           ParsedArgs object.

        keywords arguments:
            parser_factory: Callable of two parameters, a mapping of default parameters and a mapping of
                            queryable fields to their transformation functions, that returns a new callable
                            to be used as the subclass' parse_queries method. By default this is the
                            fujin.dal.query_parser factory function.
        """
        super().__init_subclass__(**kwargs)

        # set supported methods for OPTIONS support
        subcls.methods = frozenset(method for method in HTTP_METHODS if hasattr(subcls, method.lower()))

        # don't spend cpu cycles parsing the field types if this subclass
        # is an extension rather than a concrete implementation
        fields = {} if setdefaultattr(subcls, '__abstract__', False) else field_types(subcls.__doctype__)

        # these attributes can contain wildcard patterns for allowed/disallowed fields
        blacklist = getattr(subcls, '__blacklisted_fields__', ())
        whitelist = getattr(subcls, '__whitelisted_fields__', ())

        if whitelist and not blacklist:
            # assume all non-whitelisted fields are meant to be blacklisted
            blacklist = ['*']

        # these are the resolved field names of the above field patterns
        resolved_blacklist = multipattern_filter(fields, blacklist)
        resolved_whitelist = multipattern_filter(fields, whitelist)

        # blacklisted fields are applied first, so that whitelisted fields can override them
        # blacklist - whitelist gives the resolved set of blacklisted fields
        # subtracting that from the whole set of fields gives only the queryable fields
        queryable_fields = fields.keys() - (resolved_blacklist - resolved_whitelist)
        transforms = {field: transform for field, transform in fields.items() if field in queryable_fields}

        # mapping of field: default_value for queryable fields
        defaults = getattr(subcls, '__parameter_defaults__', {})

        # pagination support probably shouldn't be enabled for single datum access
        # in case the user passes an invalid, albeit unused, value that raises an error
        if setdefaultattr(subcls, '__paginate__', False):
            for key, value in DEFAULT_PAGINATION.items():
                defaults.setdefault(key, value)
                transforms[key] = positive_int

        subcls.parse_queries = parser_factory(defaults, transforms)

        subcls.__blacklisted_fields__ = frozenset(resolved_blacklist)
        subcls.__whitelisted_fields__ = frozenset(resolved_whitelist)
        subcls.__queryable_fields__ = frozenset(queryable_fields)
        subcls.__parameter_defaults__ = MappingProxyType(defaults)


    def options(self, request, **kwargs): # pylint: disable=unused-argument
        """Respond to an OPTIONS request to a resource."""
        headers = {}
        headers['Allow'] = ', '.join(self.methods)

        return HTTPResponse(headers=headers)


    async def head(self, request, **kwargs):
        """Respond to a HEAD request to a resource."""
        # responses to HEAD requests should exactly mimic the response
        # from the equivalent GET request, but without any response body
        response = await self.get(request, **kwargs)

        headers = {}
        headers['Content-Length'] = len(response.body)
        headers['Content-Type'] = response.content_type

        return HTTPResponse(headers=headers, status=response.status)


    async def get(self, request, **kwargs):
        """Respond to a GET request to a resource."""
        # data access layer is being used to request a single object in a collection
        if kwargs:
            # pagination is unused here and shouldn't be parsed, in case the supplied
            # values would cause an error
            queries = self.parse_queries(request.raw_args)
            # supplied kwargs should override any parsed query params (just in case!)
            filters = {**queries.filters, **kwargs}
            obj = await query_one(self.__doctype__, **filters)

            if not obj:
                raise RecordNotFound(filters)

            data = obj.serialize()

        # data access layer is being used to request multiple objects from a collection
        else:
            queries = self.parse_queries(request.raw_args)
            objects = await query_many(
                model=self.__doctype__,
                limit=queries.limit,
                offset=queries.offset,
                **queries.filters,
            )
            data = [obj.serialize() for obj in objects]

        return json(data)
