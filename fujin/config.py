"""
fujin.config
~~~~~~~~~~~~
Constants of values meant for configuration of the webapp and its connections.
"""
DEFAULT_PAGINATION = dict(
    limit=1000,
    offset=0,
)
