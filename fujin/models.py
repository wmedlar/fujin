"""
fujin.models
~~~~~~~~~~~~
MongoDB document models representing PAD data.
"""
from motorengine.fields import (
    BooleanField,
    DecimalField,
    EmbeddedDocumentField,
    IntField,
    ListField,
    StringField,
)

from .fields import CaseInsensitiveStringField
from .mongo import (
    Document,
    EmbeddedFieldInitializingDocument,
    embed,
    query_many,
    query_one,
)


class ActiveSkill(EmbeddedFieldInitializingDocument):
    """Document schema representing an active skill."""
    __hidden_fields__ = {'number', 'region'}
    number = IntField()
    region = CaseInsensitiveStringField()
    name = StringField()
    description = StringField()
    cooldown = embed(
        'ActiveSkillCooldown',
        min=IntField(min_value=1),
        max=IntField(),
        levels=IntField(min_value=1),
    )


class Evolution(Document):
    """Document schema representing an evolution."""
    __hidden_fields__ = {'region'}
    region = CaseInsensitiveStringField()
    base = IntField()
    from_ = IntField(db_field='from')
    to = IntField()
    type = CaseInsensitiveStringField()
    materials = ListField(IntField())


class Stat(Document):
    """Document schema representing an arbitrary stat."""
    min = IntField()
    max = IntField()
    growth = DecimalField()


class Monster(EmbeddedFieldInitializingDocument):
    """Document schema representing a monster."""
    __hidden_fields__ = {'region'}
    number = IntField()
    region = CaseInsensitiveStringField()
    name = StringField()
    attributes = ListField(CaseInsensitiveStringField())
    types = ListField(CaseInsensitiveStringField())
    rarity = IntField(min_value=1)
    cost = IntField(min_value=0)
    mp = IntField(min_value=0)
    level = embed(
        'MonsterLevel',
        max=IntField(min_value=1, max_value=99),
        curve=IntField(min_value=0),
        growth=DecimalField(min_value=0),
    )
    stats = embed(
        'MonsterStats',
        hp=EmbeddedDocumentField(Stat),
        atk=EmbeddedDocumentField(Stat),
        rcv=EmbeddedDocumentField(Stat),
    )
    sell = DecimalField(min_value=0)
    feed = DecimalField(min_value=0)
    awakenings = ListField(CaseInsensitiveStringField())
    base = IntField()
    evolutions = ListField(
        EmbeddedDocumentField(Evolution)
    )
    inheritable = BooleanField()
    skills = embed(
        'MonsterSkills',
        active=EmbeddedDocumentField(ActiveSkill),
    )

    async def resolve_embedded_references(self):
        """Query Mongo for the full object references for this document's embedded fields."""
        # while the attribute is 'from_', the db field is 'from'
        # which is a reserved keyword and cannot be used as a keyword argument
        filters = {
            'from': self.number,
            'region': self.region,
        }
        self.evolutions = await query_many(Evolution, **filters)

        # the rest of these use the same dict unpacking format for consistency
        filters = {
            'number': self.skills.active.number,
            'region': self.region,
        }
        self.skills.active = await query_one(ActiveSkill, **filters)
