"""
fujin.api
~~~~~~~~~
HTTP endpoints for retrieving PAD data.
"""
from sanic import Blueprint

from .dal import DataAccessLayer
from .models import ActiveSkill, Evolution, Monster

blueprint = Blueprint('v1')
# alias for the latest stable api endpoint
latest = blueprint


class ActiveSkills(DataAccessLayer):
    """
    Retrieve a listing of active skills in JSON format.
    """
    __doctype__ = ActiveSkill
    __paginate__ = True
    __parameter_defaults__ = dict(region='na')
    __whitelisted_fields__ = (
        'number',
        'cooldown.*',
        'region',
    )


class Evolutions(DataAccessLayer):
    """
    Retrieve a listing of evolutions in JSON format.
    """
    __doctype__ = Evolution
    __paginate__ = True
    __parameter_defaults__ = dict(region='na')


class Monsters(DataAccessLayer):
    """
    Retrieve a listing of monsters in JSON format.
    """
    __doctype__ = Monster
    __paginate__ = True
    __parameter_defaults__ = dict(region='na')
    __blacklisted_fields__ = (
        'name',
        'skills.active.name',
        'skills.active.description',
    )


class SingleMonster(DataAccessLayer):
    """
    Retrieve a single monster in JSON format.
    """
    __doctype__ = Monster
    __parameter_defaults__ = dict(region='na')
    __whitelisted_fields__ = (
        'region',
    )


blueprint.add_route(ActiveSkills.as_view(), '/active_skills')
blueprint.add_route(Evolutions.as_view(), '/evolutions')
blueprint.add_route(Monsters.as_view(), '/monsters')
blueprint.add_route(SingleMonster.as_view(), '/monsters/<number:int>')
