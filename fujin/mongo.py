"""
fujin.mongo
~~~~~~~~~~~
Setup and utilities for use with MongoDB.
"""
from collections.abc import Mapping

from motorengine.aiomotorengine import connect, Document as BaseDocument
from motorengine.fields import EmbeddedDocumentField, ListField
from sanic import Blueprint

from .utils import partition

blueprint = Blueprint('mongo')


@blueprint.listener('before_server_start')
def initialize_connection(sanic, loop, *, host=None, port=None, database=None):
    """
    Initialize a connection to the database before the server starts.

    Looks for Mongo connection parameters in the app config if none is passed
    as keyword arguments.
    """
    if host is None:
        host = sanic.config.mongo_host

    if port is None:
        port = sanic.config.mongo_port

    if database is None:
        database = sanic.config.mongo_database

    sanic.config.db = connect(database, host=host, port=port, io_loop=loop)
    patch_motorengine_default_limit()


def patch_motorengine_default_limit():
    """
    Patch `motorengine.queryset.DEFAULT_LIMIT` to None to prevent buggy default limits.

    motorengine has a bug where a queryset limit of zero does not return the
    full queryset, as per MongoDB spec. Setting the queryset default limit to
    None seems to fix this.
    """
    from motorengine import queryset
    queryset.DEFAULT_LIMIT = None


def has_id(document):
    """Determine whether a document has an ObjectID set or not."""
    return document._id is not None


def is_embedded_field(field):
    """Detect if a field is meant to contain an embedded document."""
    return isinstance(field, EmbeddedDocumentField)


def is_list_field(field):
    """Detect if a field is meant to contain a list of another field type."""
    return isinstance(field, ListField)


def field_types(doctype, *, namespace=None):
    """
    Create a mapping of fields in `doctype` to their SON conversion functions.

    If the `namespace` keyword argument is passed, each field name will be
    prefixed with its string value and a period. The `namespace` argument
    is used for recursive iteration through `doctype`'s embedded fields, if
    it has any, and only needs to be supplied if additional namespacing is
    desired.

    >>> class MonsterSkills(Document):
    ...     active = IntField()
    ...     leader = IntField()
    ...
    >>> class Monster(Document):
    ...     name = StringField()
    ...     skills = EmbeddedDocumentField(MonsterSkills)
    ...
    >>> to_son = field_types(Monster)
    >>> to_son
    {'name': <bound method BaseField.to_son of <...>>,
    'skills.active': <bound method IntField.to_son of <...>>,
    'skills.leader': <bound method IntField.to_son of <...>>}
    >>> to_son['skills.active']('123')
    123
    """
    mapping = {}

    for name, field in doctype._fields.items():
        # look up the name of the field in the database
        key = doctype._db_field_map[name]

        if namespace is not None:
            key = f'{namespace}.{key}'

        if is_embedded_field(field):
            embedded_field_types = field_types(field._embedded_document_type, namespace=key)
            mapping.update(embedded_field_types)
        elif is_list_field(field):
            mapping[key] = field._base_field.to_son
        else:
            mapping[key] = field.to_son

    return mapping


class Document(BaseDocument):
    """Custom base document class implementing a nice repr and extending serialization support."""

    __hidden_fields__ = set()


    def serialize(self, show_hidden=False):
        """
        Serialize a document to JSON.

        Fields in the `__hidden_fields__` class attribute will not be included unless `show_hidden`
        is True.
        """
        data = {}
        hidden = set() if show_hidden else self.__hidden_fields__

        for name, field in self._fields.items():
            if name in hidden:
                continue

            value = self.get_field_value(name)
            empty = field.is_empty(value)

            if field.sparse and empty:
                continue

            # embedded document field
            if not empty and hasattr(field, 'embedded_type') and hasattr(field.embedded_type, 'serialize'):
                value = value.serialize(show_hidden)
            # embedded document list field
            elif not empty and hasattr(field, 'item_type') and hasattr(field.item_type, 'serialize'):
                value = [item.serialize(show_hidden) for item in value]
            else:
                value = field.to_son(value)

            data[field.db_field] = value

        return data


    def __repr__(self):
        pairs = (f'{key}={value!r}' for key, value in self._values.items())
        keywords = ', '.join(pairs)
        class_name = type(self).__qualname__
        return f'{class_name}({keywords})'


class EmbeddedFieldInitializingDocument(Document):
    """
    Document subclass that overrides the constructor to initialize any
    embedded fields not provided.

    If no value is passed to the constructor for an embedded field, the
    field will be initialzed as an empty object.
    >>> mon = Monster(id=752)
    >>> mon.name._values
    {'en': None, 'jp': None}

    If a dictionary is passed to the constructor for an embedded field,
    the field will be initialized using it as kwargs.
    >>> mon = Monster(id=752, name=dict(en='Incarnation of Kirin, Sakuya'))
    >>> mon.name._values
    {'en': 'Incarnation of Kirin, Sakuya', 'jp': None}
    Undefined keys are treated the same as any other kwarg and created
    as a DynamicField prefixed in the collection with an underscore.

    If any other value is passed to the constructor for an embedded field,
    the field will take that value and forego initialization. This is useful
    if want to prevent initialization of a particular field and maintain a
    sparse index.
    >>> mon = Monster(id=752, name=None)
    >>> print(mon.name)
    None
    """

    def __init__(self, **kwargs):

        for name, field in self._fields.items():
            if is_embedded_field(field):
                value = kwargs.get(name, {})

                if isinstance(value, Mapping):
                    kwargs[name] = field.embedded_type(**value)

        super().__init__(**kwargs)


def embed(name, **fields):
    """
    Create a new embedded document field from keyword arguments.

    This function is useful to replace single-use classes for embedded fields.
    For instance, this document mapping:
    >>> class MonsterName(Document):
    ...    en = StringField()
    ...    jp = StringField()
    ...
    >>> class Monster(Document):
    ...    name = EmbeddedDocumentField(MonsterName)
    ...
    can be rewritten in a single class with:
    >>> class Monster(Document):
    ...    name = embed('MonsterName',
    ...        en=StringField(),
    ...        jp=StringField(),
    ...    )
    ...

    This function even works for deeply-nested fields, by creating a subclass of
    EmbeddedFieldInitializingDocument when any included fields are instances of
    EmbeddedDocumentField.
    """
    has_embedded_fields = any(is_embedded_field(field) for field in fields.values())
    parent = EmbeddedFieldInitializingDocument if has_embedded_fields else Document
    cls = type(name, (parent,), fields)
    return EmbeddedDocumentField(cls)


async def save_many(model, documents, alias=None):
    """
    Bulk insert/update `documents` of type `model`.

    This function makes use of Motor's unordered bulk operations
    (http://motor.readthedocs.io/en/stable/examples/bulk.html#unordered-bulk-write-operations)
    to minimize network overhead when inserting/updating many
    documents. Because it references the collection object directly,
    this function bypasses motorengine's document validation -- all
    validation is performed on the server.

    This function returns a dictionary of operation metrics, which
    may include errors while writing.
    """
    bulk = model.objects.coll(alias).initialize_unordered_bulk_op()
    updates, inserts = partition(has_id, documents)

    for doc in updates:
        find = dict(_id=doc._id)
        bulk.find(find).replace_one(doc.to_son())

    for doc in inserts:
        bulk.insert(doc.to_son())

    stats = await bulk.execute()
    simplified_stats = dict(
        added=stats['nInserted'],
        updated=stats['nModified'],
        errors=stats['writeErrors'],
    )

    return simplified_stats


async def query_many(model, *, limit=None, offset=None, **filters):
    """Retrieve multiple matching objects from a collection."""
    query = (model.objects
                  .filter(filters)
                  .skip(offset)
                  .limit(limit))
    return await query.find_all()


async def query_one(model, *, fields=None, **filters):
    """Retrieve a single matching object from a collection."""
    fields = fields or ()
    query = (model.objects
                  .only(*fields)
                  .get(**filters))
    return await query


async def update_id(document, model=None, **filters):
    """
    Update a document with an existing BSON ID.

    This function passes arbitrary filters to funin.mongo.query_one to retrieve
    a matching document's id. If a matching document is found, its _id attribute
    is copied to the new document, otherwise the new document is returned unchanged.
    """
    if model is None:
        model = type(document)

    obj = await query_one(model, fields=['_id'], **filters)

    if obj is not None:
        document._id = obj._id

    return document
