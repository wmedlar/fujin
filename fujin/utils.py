"""
fujin.utils
~~~~~~~~~~~
Miscellaneous helper functions that don't have a home elsewhere.
"""
from enum import IntEnum
from fnmatch import filter as wildcard_filter
from itertools import chain, filterfalse, tee

from sanic.response import json


def json_response(message, data=None, *, status=200, success=None):
    """
    Build a structured Sanic JSON response.

    >>> json_response('missing request parameter', dict(parameter='id'), status=400)
    {
        "message": "missing request parameter",
        "parameter": "id",
        "success": false
    }
    """
    if success is None:
        success = status < 400

    response = dict(
        message=message,
        success=success,
    )

    if data is not None:
        response.update(data)

    return json(response, status=status)


def multipattern_filter(strings, patterns):
    """
    Filter strings that match any of the Unix shell-style wildcard patterns.

    See the documentation for fnmatch for more information about patterns:
    https://docs.python.org/3/library/fnmatch.html
    """
    it = chain.from_iterable(
        wildcard_filter(strings, pattern) for pattern in patterns
    )
    # remove duplicates
    return set(it)


def partition(predicate, iterable):
    """
    Partition an iterable into true and false entries.

    Code shamelessly stolen and slightly modified from the itertools documentation.

    >>> odds, evens = partition(is_odd, range(10))
    >>> list(odds)
    [1, 3, 5, 7, 9]
    >>> list(evens)
    [0, 2, 4, 6, 8]
    """
    t1, t2 = tee(iterable)
    return filter(predicate, t1), filterfalse(predicate, t2)


def positive_int(value, nonpositive=None):
    """
    Parse a value a positive integer or None.

    Returns the integer value of `value` when it is positive (i.e., greater
    than zero), and `nonpositive` otherwise. `nonpositive` defaults to None.

    Raises:
        ValueError when value cannot be parsed as an integer.
        TypeError when value is not a number, string, or bytes-like object.
    """
    try:
        i = int(value)
    except (ValueError, TypeError):
        raise

    return i if i > 0 else nonpositive


def setdefaultattr(obj, attr, default=None):
    """
    Retrieve an attribute from an object, setting it to a default if it does not exist.

    Essentially dict.setdefault for attributes.
    """
    value = getattr(obj, attr, default)

    if value is default:
        setattr(obj, attr, default)

    return value


class LowerCaseIntEnum(IntEnum):
    """IntEnum subclass that implements a __str__ method to return its member name in lower case."""

    def __str__(self):
        # pylint: disable=no-member
        return self.name.lower()
