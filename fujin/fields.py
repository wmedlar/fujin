"""
fujin.fields
~~~~~~~~~~~~
Custom fields for use with MongoDB document models.
"""
from motorengine.fields import StringField


class CaseInsensitiveStringField(StringField):
    """Field responsible for storing casefolded text."""

    def to_son(self, value):
        """Convert non-null values to casefolded strings."""
        return value if value is None else str(value).casefold()
