"""
fujin.app
~~~~~~~~~
Creation and configuration of the Sanic webapp.
"""
from sanic import Sanic

from .api import blueprint as api_blueprint, latest as latest_api_blueprint
from .exceptions import APIErrorHandler
from .healthcheck import blueprint as healthcheck_blueprint
from .mongo import blueprint as mongo_blueprint

app = Sanic()
app.error_handler = APIErrorHandler()
app.blueprint(mongo_blueprint)
# latest stable api version should be accessible at / and /<version>
app.blueprint(latest_api_blueprint)
app.blueprint(api_blueprint, url_prefix='/v1')
app.blueprint(healthcheck_blueprint)
