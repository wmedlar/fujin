"""
fujin.etl.utils
~~~~~~~~~~~~~~~
Utilities for simplifying ETL code.
"""
from asyncio import get_event_loop
from functools import partial, wraps

import ijson


def take(iterator, n=1):
    """Takes `n` elements from `iterator`."""
    # don't create a copy so the original iterator is consumed
    for _ in range(n):
        yield next(iterator)


def discard(iterator, n=1):
    """Discards `n` elements of `iterator`."""
    for _ in take(iterator, n):
        pass


def iterjson(path, key):
    """Iteratively read JSON objects from file."""
    with open(path) as f:
        yield from ijson.items(f, key)


def iterlen(iterator):
    """Calculates the number of items in an iterator, exhausting it in the process."""
    return sum(1 for _ in iterator)


def use_default_loop(func=None, *, loop_kwarg='loop'):
    """
    Decorate a function to supply the default event loop as a keyword argument
    in the event that one is not passed.

    This decorator is intended to replace the idiom
    `loop = loop or asyncio.get_event_loop()` in a function with a signature
    similar to `def func(a, b, c, *, loop=None)`.

    The keyword argument name to pass the loop in as defaults to 'loop', but
    can be changed via the `loop_kwarg` parameter.
    """

    if func is None:
        return partial(use_default_loop, loop_kwarg=loop_kwarg)

    @wraps(func)
    def inner(*args, **kwargs):
        # pylint: disable=missing-docstring
        loop = get_event_loop()
        kwargs.setdefault(loop_kwarg, loop)
        return func(*args, **kwargs)

    return inner
