"""
fujin.etl
~~~~~~~~~
Functions for the extraction, transformation, and loading of canonical data sources into MongoDB.
"""
from . import utils
from .extract import parse_active, parse_evolutions, parse_monster
from .load import ingest_actives, ingest_evolutions, ingest_monsters
from .validate import is_valid_active, is_valid_evolution, is_valid_monster
