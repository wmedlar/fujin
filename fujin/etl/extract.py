"""
fujin.etl.extract
~~~~~~~~~~~~~~~~~
Functions for parsing canonical PAD data structures.
"""
# pylint: disable=too-many-locals
from decimal import Decimal
from itertools import product

from .utils import discard, take
from ..models import ActiveSkill, Evolution, Monster
from ..constants import Attribute, Awakening, EvolutionType, Type


def parse_active(mapping, **metadata):
    """
    Parse a canonical skill mapping into an ActiveSkill document.

    args:
        mapping: A canonical skill data structure. See docs on the
                 download_skill_data endpoint for more information.

        **kwargs: Key-value pairs containing metadata to include in
                  the parsed document -- skill region, for example.

    raises:
        KeyError: If `mapping` is missing an expected key, i.e., an invalid
        data structure.

    returns:
        fujin.models.ActiveSkill
    """
    skill = ActiveSkill(**metadata)
    skill.name = mapping['name']
    skill.description = mapping['help']
    skill.cooldown.max = mapping['ctbs']
    skill.cooldown.levels = mapping['lcap']
    # minimum turns (i.e., skill at max level) are inferred from max turns - skill levels
    # skill levels are one-indexed, thus the + 1
    skill.cooldown.min = skill.cooldown.max - skill.cooldown.levels + 1

    return skill


def parse_evolutions(array, **metadata):
    """
    Parse a canonical card array into a list of Evolution documents.

    args:
        array: A canonical card data array from PAD v9.2 onward. See
        docs on the download_card_data endpoint for more information.

        **kwargs: Key-value pairs containing metadata to include in
                  the parsed document -- evolution region, for example.

    raises:
        StopIteration: If `array` is not long enough, e.g., an invalid
                       data structure.

    returns:
        list of fujin.models.Evolution objects

        If the parsed evolution is reversible (i.e., an ultimate evo),
        a document of the reverse evolution will be included in the
        returned list.
    """
    it = iter(array)

    evo = Evolution(**metadata)
    evo.to = next(it)

    # discard name and attributes
    discard(it, n=3)

    # bit is set if this is an ult evo
    evo.type = EvolutionType.ULTIMATE if next(it) else EvolutionType.STANDARD

    # discard other irrelevant data
    discard(it, n=35)

    evo.from_ = next(it)
    evo.materials = [id for id in take(it, n=5) if id > 0]

    # this is the same for every single evo, even ones that can't reverse
    # but we'll parse it dynamically just in case that ever changes
    reverse_materials = [id for id in take(it, n=5) if id > 0]

    # skip unknowns, enemy skills, and awakenings
    discard(it, n=6)
    n_enemy_skills = next(it)

    for _ in range(n_enemy_skills):
        discard(it, n=3)

    n_awakenings = next(it)
    discard(it, n=n_awakenings)

    # lowest form of evolution chain
    evo.base = next(it)

    evolutions = [evo]

    if evo.type == EvolutionType.ULTIMATE:
        reverse = Evolution(
            to=evo.from_,
            from_=evo.to,
            type=EvolutionType.REVERSE,
            materials=reverse_materials,
            base=evo.base,
            **metadata,
        )
        evolutions.append(reverse)

    return evolutions


def parse_monster(array, **metadata):
    """
    Parse a canonical card array into a Monster document.

    args:
        array: A canonical card data structure from PAD v9.2 onward.
               See docs on the download_card_data endpoint for more
               information.

        **kwargs: Key-value pairs containing metadata to include in
                  the parsed document -- evolution region, for example.

    raises:
        StopIteration: If `array` is not long enough, e.g., an invalid
                       data structure.

    returns:
        fujin.models.Monster
    """
    it = iter(array)

    mon = Monster(**metadata)
    mon.number = next(it)
    mon.name = next(it)
    mon.attributes = [Attribute(id) for id in take(it, n=2) if id > -1]

    # discard ultimate flag (used in evolutions)
    discard(it, n=1)

    # see below for third type
    types = list(take(it, n=2))

    mon.rarity = next(it)
    mon.cost = next(it)

    # discard ?
    discard(it, n=1)

    mon.level.max = next(it)
    # feed xp is stored at lv 4
    mon.feed = Decimal(next(it)) / 4

    # discard ?
    discard(it, n=1)

    # sell price is stored at lv 10
    mon.sell = Decimal(next(it)) / 10

    stats = 'hp', 'atk', 'rcv'
    params = 'min', 'max', 'growth'

    for stat, param in product(stats, params):
        stat_obj = getattr(mon.stats, stat)
        setattr(stat_obj, param, next(it))

    mon.level.curve = next(it)
    mon.level.growth = Decimal(next(it))

    # 0 indicates absence of a skill, replace with a null value
    mon.skills.active.number = next(it) or None
    leader = next(it) or None # pylint: disable=unused-variable

    # skip all enemy data
    discard(it, n=13)
    # skip evolutions
    discard(it, n=11)
    # skip unknowns
    discard(it, n=6)

    # skip enemy skills
    n_enemy_skills = next(it)

    for _ in range(n_enemy_skills):
        discard(it, n=3)

    n_awakenings = next(it)

    mon.awakenings = [Awakening(id) for id in take(it, n=n_awakenings)] # pylint: disable=not-callable

    # discard base form (used in evolutions)
    mon.base = next(it)
    # discard unknown flag
    discard(it, n=1)

    # tertiary type
    types.append(next(it))
    mon.types = [Type(id) for id in types if id > -1]

    mon.mp = next(it)

    # discard latent type
    discard(it, n=1)
    # discard ?
    discard(it, n=1)

    # skill inheritance info is packed into three bits
    # LSB is set if the card meets the SI requirements -> 5*, REM, has active, final form
    # middle bit is set if the card has been released
    # MSB is set if the card is a collab monster
    # only LSB must be set for us to consider a card inheritable
    mon.inheritable = bool(next(it) & 0b001)

    return mon
