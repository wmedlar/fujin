"""
fujin.etl.validate
~~~~~~~~~~~~~~~~~~
Functions for the validation of canonical PAD data structures.
"""

VALID_MONSTER_ID_RANGE = range(1, 10000)
INVALID_MONSTER_NAME_PREFIXES = (
    '?',
    '*',
    'Alt.'
)


def is_active(mapping):
    """
    Determine whether a canonical skill mapping is an active skill.
    """
    return mapping['ctel'] == -1


def is_valid_active(active):
    """
    Validate whether an active skill is real and usable.
    """
    # seemingly the only difference between valid and invalid actives is the
    # presence of a truthy description
    valid = bool(active.description)
    return valid


def is_valid_evolution(evolution):
    """
    Validate whether an evolution is real and possible.
    """
    # dummy alt monsters contain evolution information, but with a base form of 0
    # additional rules will catch any bad eggs that would otherwise slip through
    valid = (
        evolution.base in VALID_MONSTER_ID_RANGE
        and evolution.to in VALID_MONSTER_ID_RANGE
        and evolution.from_ in VALID_MONSTER_ID_RANGE
    )
    return valid


def is_valid_monster(monster):
    """
    Validate whether a monster is real and obtainable.
    """
    valid = (
        monster.mp > 0
        and monster.number in VALID_MONSTER_ID_RANGE
        and not monster.name.startswith(INVALID_MONSTER_NAME_PREFIXES)
    )
    return valid
