FROM python:3.6-alpine3.6

ENV PYTHONDONTWRITEBYTECODE true

WORKDIR /app

# install entr util for hot-reload
RUN apk add --no-cache --virtual .runtime-dependencies \
        entr

RUN apk add --no-cache --virtual .build-dependencies \
        g++ \
        git \
        libffi-dev \
        make

# copy and install dependencies separately to cache this step
# so each time we make a change to an unrelated file we won't have to reinstall everything
COPY requirements.txt .
RUN pip install -r requirements.txt && apk del .build-dependencies

COPY . .
RUN pip install -e .

EXPOSE 8080
ENTRYPOINT ["./docker-entrypoint.sh"]
