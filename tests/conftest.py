from motorengine.fields import BooleanField, EmbeddedDocumentField, IntField, StringField
from pytest import fixture

from fujin.mongo import Document


@fixture(scope='module')
def doctype():
    class TestEmbeddedDocument(Document):
        a = StringField()
        b = StringField()
        c = StringField()

    class TestDocument(Document):
        bool  = BooleanField()
        int   = IntField()
        str   = StringField()
        embed = EmbeddedDocumentField(TestEmbeddedDocument)

    return TestDocument


@fixture(scope='module')
def document(doctype):
    obj = doctype(
        bool=True,
        int=1,
        str='2',
        embed=doctype.embed.embedded_type(a='a', b='b', c='c'),
    )
    return obj
