from pytest import mark, raises

from fujin.config import DEFAULT_PAGINATION
from fujin.dal import query_parser, DataAccessLayer, ParsedArgs
from fujin.exceptions import InvalidQueryParameter


class TestQueryParser:

    DEFAULTS = dict(a=1, b='2', c=True, limit=1000, offset=0)
    TRANSFORMS = dict(a=int, b=str, c=bool, limit=int, offset=int)
    PARSE_QUERIES = query_parser(DEFAULTS, TRANSFORMS)


    @mark.parametrize('queries,expected', [
        ({}, ParsedArgs({'a': 1, 'b': '2', 'c': True}, 1000, 0)),
        ({'a': '3', 'b': 'foo'}, ParsedArgs({'a': 3, 'b': 'foo', 'c': True}, 1000, 0)),
        ({'c': ''}, ParsedArgs({'a': 1, 'b': '2', 'c': False}, 1000, 0)),
        ({'limit': '123', 'offset': '456'}, ParsedArgs({'a': 1, 'b': '2', 'c': True}, 123, 456)),
        ({'a': 'abc'}, InvalidQueryParameter('a', 'abc')),
        ({'limit': 'def'}, InvalidQueryParameter('limit', 'def')),
        ({'offset': '0x001'}, InvalidQueryParameter('offset', '0x001')),
    ])
    def test_query_parser_factory(self, queries, expected):

        if isinstance(expected, InvalidQueryParameter):
            with raises(type(expected)) as exc_info:
                self.PARSE_QUERIES(queries)

            assert exc_info.value.key == expected.key
            assert exc_info.value.value == expected.value

        else:
            assert self.PARSE_QUERIES(queries) == expected


class TestDataAccessLayer:

    QUERIES = {'bool': '', 'int': '1', 'embed.b': '2', 'limit': '3', 'offset': '4', 'unused': 'not used'}
    SUPPORTED_METHODS = {'GET', 'HEAD', 'OPTIONS'}


    def test_abstract(self):
        class AbstractTestView(DataAccessLayer):
            __abstract__ = True

        assert AbstractTestView.methods == self.SUPPORTED_METHODS
        assert callable(AbstractTestView.parse_queries)
        assert AbstractTestView.__abstract__
        assert AbstractTestView.__blacklisted_fields__ == set()
        assert AbstractTestView.__whitelisted_fields__ == set()
        assert AbstractTestView.__queryable_fields__ == set()
        assert AbstractTestView.__parameter_defaults__ == {}

        with raises(TypeError):
            AbstractTestView()


    def test_init_subclass(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert not TestView.__paginate__
        assert TestView.__blacklisted_fields__ == set()
        assert TestView.__whitelisted_fields__ == set()
        assert TestView.__queryable_fields__ == {'bool', 'int', 'str', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__parameter_defaults__ == {}

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False, 'int': 1, 'embed.b': '2'}
        assert parsed.limit is None
        assert parsed.offset is None


    def test_pagination(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype
            __paginate__ = True

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert TestView.__paginate__
        assert TestView.__blacklisted_fields__ == set()
        assert TestView.__whitelisted_fields__ == set()
        assert TestView.__queryable_fields__ == {'bool', 'int', 'str', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__parameter_defaults__ == DEFAULT_PAGINATION

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False, 'int': 1, 'embed.b': '2'}
        assert parsed.limit == 3
        assert parsed.offset == 4


    def test_blacklist(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype
            __blacklisted_fields__ = ('int', 'embed.*')

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert not TestView.__paginate__
        assert TestView.__blacklisted_fields__ == {'int', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__whitelisted_fields__ == set()
        assert TestView.__queryable_fields__ == {'bool', 'str'}
        assert TestView.__parameter_defaults__ == {}

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False}
        assert parsed.limit is None
        assert parsed.offset is None


    def test_whitelist(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype
            __whitelisted_fields__ = ('bool', '*.b')

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert not TestView.__paginate__
        assert TestView.__blacklisted_fields__ == {'bool', 'int', 'str', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__whitelisted_fields__ == {'bool', 'embed.b'}
        assert TestView.__queryable_fields__ == {'bool', 'embed.b'}
        assert TestView.__parameter_defaults__ == {}

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False, 'embed.b': '2'}
        assert parsed.limit is None
        assert parsed.offset is None


    def test_blacklist_with_whitelist(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype
            __blacklisted_fields__ = ('int', 'embed.*')
            __whitelisted_fields__ = ('bool', 'embed.b')

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert not TestView.__paginate__
        assert TestView.__blacklisted_fields__ == {'int', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__whitelisted_fields__ == {'bool', 'embed.b'}
        assert TestView.__queryable_fields__ == {'bool', 'str', 'embed.b'}
        assert TestView.__parameter_defaults__ == {}

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False, 'embed.b': '2'}
        assert parsed.limit is None
        assert parsed.offset is None


    def test_parameter_defaults(self, doctype):
        class TestView(DataAccessLayer):
            __doctype__ = doctype
            __parameter_defaults__ = {'str': 'foo'}

        assert TestView.methods == self.SUPPORTED_METHODS
        assert callable(TestView.parse_queries)
        assert not TestView.__abstract__
        assert not TestView.__paginate__
        assert TestView.__blacklisted_fields__ == set()
        assert TestView.__whitelisted_fields__ == set()
        assert TestView.__queryable_fields__ == {'bool', 'int', 'str', 'embed.a', 'embed.b', 'embed.c'}
        assert TestView.__parameter_defaults__ == {'str': 'foo'}

        parsed = TestView.parse_queries(self.QUERIES)

        assert parsed.filters == {'bool': False, 'int': 1, 'embed.b': '2', 'str': 'foo'}
        assert parsed.limit is None
        assert parsed.offset is None
