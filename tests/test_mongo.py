class TestDocument:

    def test_serialize(self, document):
        all_fields = dict(bool=True, int=1, str='2', embed={'a': 'a', 'b': 'b', 'c': 'c'})
        no_hidden_fields = dict(int=1, str='2')

        assert document.serialize() == all_fields
        assert document.serialize(show_hidden=True) == all_fields

        type(document).__hidden_fields__ = {'bool', 'embed'}

        assert document.serialize() == no_hidden_fields
        assert document.serialize(show_hidden=True) == all_fields
