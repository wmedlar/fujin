from inspect import isclass
from random import randint
from types import SimpleNamespace

from pytest import mark, raises

from fujin.utils import (
    multipattern_filter,
    partition,
    positive_int,
    setdefaultattr,
    LowerCaseIntEnum,
)

# unique object for testing equality
OBJECT = object()


class TestMultiPatternFilter:

    @mark.parametrize('patterns,expected', [
        (['*'], {'a', 'b', 'c.1', 'c.2', 'c.3', 'c.4', 'd.1.1', 'd.1.2', 'd.2.1', 'd.3.1', 'd.3.2'}),
        ([], set()),
        (['a', 'b', 'c'], {'a', 'b'}),
        (['a', 'b', 'c.*'], {'a', 'b', 'c.1', 'c.2', 'c.3', 'c.4'}),
        (['c.*.*'], set()),
        (['d.*.2'], {'d.1.2', 'd.3.2'}),
    ])
    def test_multipattern_filter(self, patterns, expected):
        strings = ('a', 'b', 'c.1', 'c.2', 'c.3', 'c.4', 'd.1.1', 'd.1.2', 'd.2.1', 'd.3.1', 'd.3.2')
        assert multipattern_filter(strings, patterns) == expected


class TestPartition:

    def test_partition(self):
        bools = [bool(randint(0, 1)) for i in range(100)]
        expected_trues = sum(bools)
        expected_falses = len(bools) - expected_trues

        trues, falses = partition(bool, bools)
        # partition returns two iterators that must be cast to lists
        trues, falses = list(trues), list(falses)

        assert len(trues) == expected_trues
        assert len(falses) == expected_falses
        # all in trues are True
        assert all(trues)
        # all in falses are False
        assert not any(falses)


class TestPositiveInt:

    @mark.parametrize('value,nonpositive,expected', [
        (1, None, 1),
        (0, None, None),
        (-1, None, None),
        (1.1, None, 1),
        (0.0, None, None),
        (-1.1, None, None),
        ('1', None, 1),
        ('0', None, None),
        ('-1', None, None),
        ('1.1', None, ValueError),
        ('0.0', None, ValueError),
        ('-1.1', None, ValueError),
        ('0b0101', None, ValueError),
        (None, None, TypeError),
        (object(), None, TypeError),
        (1, OBJECT, 1),
        (0, OBJECT, OBJECT),
        (-1, OBJECT, OBJECT),
        (1.1, None, 1),
        (0.0, None, None),
        (-1.1, None, None),
        ('1', None, 1),
        ('0', OBJECT, OBJECT),
        ('-1', OBJECT, OBJECT),
        ('1.1', OBJECT, ValueError),
        ('0.0', OBJECT, ValueError),
        ('-1.1', OBJECT, ValueError),
        ('0b0101', OBJECT, ValueError),
        (object(), OBJECT, TypeError),
    ])
    def test_positive_int(self, value, nonpositive, expected):
        if isclass(expected) and issubclass(expected, Exception):
            with raises(expected):
                positive_int(value, nonpositive)

        else:
            assert positive_int(value, nonpositive) == expected


class TestSetDefaultAttr:

    @mark.parametrize('default', [
        'abc',
        3,
        3.3,
        None,
        OBJECT,
    ])
    def test_attr_exists(self, default):
        inst = SimpleNamespace(foo='bar')
        assert inst.foo == 'bar' # sanity check
        assert setdefaultattr(inst, 'foo', default) is inst.foo


    @mark.parametrize('default', [
        'abc',
        3,
        3.3,
        None,
        OBJECT,
    ])
    def test_attr_not_exists(self, default):
        inst = SimpleNamespace()
        assert not hasattr(inst, 'foo') # sanity check
        assert setdefaultattr(inst, 'foo', default) is default
        assert inst.foo is default


class TestLowerCaseIntEnum:

    def test_subclass(self):
        class TestEnum(LowerCaseIntEnum):
            pass

        assert issubclass(TestEnum, int)
        assert TestEnum._member_type_ is int


    def test_lowercase_names(self):
        class TestEnum(LowerCaseIntEnum):
            UPPER = 0
            lower = 1
            MiXeD = 2

        # sanity check
        assert TestEnum._member_map_

        for member in TestEnum:
            assert str(member).islower()


    @mark.parametrize('value', [
        'abc',
        3,
        3.3,
        None,
        OBJECT,
    ])
    def test_invalid(self, value):
        class TestEnum(LowerCaseIntEnum):
            UPPER = 0
            lower = 1
            MiXeD = 2

        # sanity check
        assert value not in TestEnum

        with raises(ValueError, message=f'{value} is not a valid TestEnum'):
            TestEnum(value)
