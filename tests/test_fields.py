from pytest import mark

from fujin.fields import CaseInsensitiveStringField


class TestCaseInsensitiveStringField:


    @mark.parametrize('value,son', [
        ('UPPER', 'upper'),
        ('lower', 'lower'),
        ('MiXeD', 'mixed'),
        ('ß', 'ss'),
        ('', ''),
        (0, '0'),
        (1, '1'),
        (1.1, '1.1'),
        (None, None),
    ])
    def test_serialization(self, value, son):
        field = CaseInsensitiveStringField()
        assert field.to_son(value) == son


    @mark.parametrize('son,value', [
        ('UPPER', 'UPPER'),
        ('lower', 'lower'),
        ('MiXeD', 'MiXeD'),
        ('ß', 'ß'),
        ('ss', 'ss'),
        ('', ''),
        ('0', '0'),
        ('1', '1'),
        ('1.1', '1.1'),
        (None, None),
    ])
    def test_deserialization(self, son, value):
        field = CaseInsensitiveStringField()
        assert field.from_son(son) == value
